var body, horse, horse_element, i, legs, puts, ref, scrollPercent;

horse = "  ,\n_,,)\\.~,,._\n(()`  ``)\\))),,_\n |     \\ ''((\\)))),,_          ____\n |6`   |   ''((\\())) \"-.____.-\"    `-.-,\n |    .'\\    ''))))'                  \\)))\n |   |   `.     ''                     ((((\n \\, _)     \\/                          |))))\n  `'        |                          (((((\n            \\                  |       ))))))\n             `|    |           ,\\     /((((((\n              |   / `-.______.<  \\   |  )))))\n              |   |  /         `. \\  \\  ((((\n              |  / \\ |           `.\\  | (((\n              \\  | | |             )| |  ))\n               | | | |             || |  '";

legs = "\n               | | | |             || |";

puts = console.log;

body = document.body;

Element.prototype.addElement = function(name, content = "") {
  var elm;
  elm = document.createElement(name);
  elm.innerHTML = content;
  this.appendChild(elm);
  return elm;
};


horse_element = body.addElement("pre", horse)
horse_element = document.getElementsByTagName("pre")[0];

horse_element.insertAdjacentHTML("beforeend", legs);

for (i = 0, ref = Math.round((window.innerHeight - body.offsetHeight) * 0.1); (0 <= ref ? i <= ref : i >= ref); 0 <= ref ? i++ : i--) {
  horse_element.insertAdjacentHTML("beforeend", legs);
}

scrollPercent = function() {
  var b, h, sh, st;
  h = document.documentElement;
  b = document.body;
  st = 'scrollTop';
  sh = 'scrollHeight';
  return (h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight) * 100;
};

document.addEventListener('scroll', function() {
  var j, ref1, results;
  // puts "#{scrollPercent()} : #{Math.round document.documentElement.clientHeight / scrollPercent()}"
  if (scrollPercent() >= 75) {
    results = [];
    for (j = 0, ref1 = Math.round(document.documentElement.clientHeight / scrollPercent()); (0 <= ref1 ? j <= ref1 : j >= ref1); 0 <= ref1 ? j++ : j--) {
      results.push(horse_element.insertAdjacentHTML("beforeend", legs));
    }
    return results;
  }
});